package qacube.internship.Backend.resolver.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qacube.internship.Backend.model.QualifyingResult;
import qacube.internship.Backend.model.SeasonQualResDTO;
import qacube.internship.Backend.Service.QualifyingResultsService;
import qacube.internship.Backend.Utility.CalculateResults;

import java.util.List;
@Component
public class ResultsResolver implements GraphQLQueryResolver {

    @Autowired
    private QualifyingResultsService qualifyingResultsService;

    public SeasonQualResDTO qualifyingResultsBySeason(int season) {

        List<QualifyingResult> results = this.qualifyingResultsService.getResultsForSeason(season);
        SeasonQualResDTO seasonInfo = new SeasonQualResDTO();

        seasonInfo.setAverageTime(CalculateResults.CalculateAverageTime(results));
        seasonInfo.setMinimumTime(CalculateResults.CalculateMinimumTime(results));
        seasonInfo.setMaximumTime(CalculateResults.CalculateMaximumTime(results));
        return seasonInfo;

    }

}
