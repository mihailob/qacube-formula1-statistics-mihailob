package qacube.internship.Backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import qacube.internship.Backend.model.Circuit;

@Repository
public interface CircuitRepository extends JpaRepository<Circuit, Long> {
    Circuit findByCircuitId(String circuitId);
}
