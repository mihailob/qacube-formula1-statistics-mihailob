package qacube.internship.Backend.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import qacube.internship.Backend.model.Constructor;
import qacube.internship.Backend.model.Driver;
import qacube.internship.Backend.model.QualifyingResult;
import qacube.internship.Backend.model.Race;
import qacube.internship.Backend.Service.ConstructorService;
import qacube.internship.Backend.Service.DriverService;
import qacube.internship.Backend.Service.RaceService;

import java.util.List;
import java.util.Map;

@Component
public class QualifyingResultsResource {

    private static final String API_PREFIX = "https://ergast.com/api/f1/";

    private static final String API_QUALIFYING_POSTFIX = "/qualifying.json";

    @Autowired
    private RaceService raceService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private ConstructorService constructorService;

    @Order(value = 4)
    @EventListener(ApplicationReadyEvent.class)
    public void loadRacesAndQualifications(){

        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();

        for(int i = 2015; i<=2017; i++){
            ResponseEntity<Map> response =
                    restTemplate.getForEntity(
                            API_PREFIX + i + API_QUALIFYING_POSTFIX,
                            Map.class);

            Map<String, Object> mrData = (Map<String, Object>) response.getBody().get("MRData");
            Map<String, Object> raceTable = (Map<String, Object>) mrData.get("RaceTable");
            List<Object> races = (List<Object>) raceTable.get("Races");

            for(Object d: races){
                Race newRace = mapper.convertValue(d, Race.class);

                for(QualifyingResult qr : newRace.getQualifyingResults()){
                    // set driver for given qualifying result if it already exists in db
                    if(driverService.findByCode(qr.getDriver().getCode()) != null){
                        Driver existingDriver = driverService.findByCode(qr.getDriver().getCode());
                        qr.setDriver(existingDriver);
                    }
                    // set constructor for given qualifying result if it already exists in db
                    if(constructorService.findByConstructorId(qr.getConstructor().getConstructorId()) != null){
                        Constructor existingConst = constructorService.findByConstructorId(qr.getConstructor().getConstructorId());
                        qr.setConstructor(existingConst);
                    }
                }
                raceService.CreateRace(newRace);
            }
        }
    }
}
