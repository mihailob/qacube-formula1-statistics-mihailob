package qacube.internship.Backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import qacube.internship.Backend.model.RaceResult;

@Repository
public interface RaceResultRepository extends JpaRepository<RaceResult, Long> {
}
