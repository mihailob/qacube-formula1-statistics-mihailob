package qacube.internship.Backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Race {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int season;

    private int round;

    private String url;

    private String raceName;

    @JsonProperty(value = "Circuit")
    @ManyToOne(cascade = CascadeType.ALL)
    private Circuit circuit;

    @JsonProperty(value = "QualifyingResults")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<QualifyingResult> qualifyingResults;

    @OneToMany
    private Collection<RaceResult> raceResults;

    private String date;

    private String time;

}
