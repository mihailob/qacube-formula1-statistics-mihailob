package qacube.internship.Backend.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import qacube.internship.Backend.model.Driver;
import qacube.internship.Backend.Service.DriverService;

import java.util.List;
import java.util.Map;

@Component
public class DriverResource {

    private static final String API_PREFIX = "https://ergast.com/api/f1/";

    private static final String API_DRIVERS_POSTFIX = "/drivers.json";

    @Autowired
    private DriverService driverService;

    @Order(value = 1)
    @EventListener(ApplicationReadyEvent.class)
    public void loadDrivers(){

        RestTemplate restTemplate = new RestTemplate();

        ObjectMapper mapper = new ObjectMapper();

        for(int i = 2015; i<=2017; i++){
            ResponseEntity<Map> response =
                    restTemplate.getForEntity(
                            API_PREFIX + i + API_DRIVERS_POSTFIX,
                            Map.class);

            Map<String, Object> mrData = (Map<String, Object>) response.getBody().get("MRData");
            Map<String, Object> driverTable = (Map<String, Object>) mrData.get("DriverTable");
            List<Object> drivers = (List<Object>) driverTable.get("Drivers");

            for(Object d: drivers){
                Driver newDriver = mapper.convertValue(d, Driver.class);
                if(driverService.findByCode(newDriver.getCode()) == null){
                    driverService.CreateDriver(newDriver);
                }
            }
        }
    }

}
