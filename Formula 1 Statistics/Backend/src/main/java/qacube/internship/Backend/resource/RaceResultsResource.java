package qacube.internship.Backend.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import qacube.internship.Backend.model.RaceResult;
import qacube.internship.Backend.Service.RaceResultsService;

import java.util.List;
import java.util.Map;

@Component
public class RaceResultsResource {

    @Autowired
    private RaceResultsService resultsService;

    //@EventListener(ApplicationReadyEvent.class)
    public void loadRaceResults(){

        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();

        for(int i = 2015; i<=2017; i++){
            ResponseEntity<Map> response =
                    restTemplate.getForEntity(
                            "https://ergast.com/api/f1/" + i + "/results.json",
                            Map.class);

            Map<String, Object> mrData = (Map<String, Object>) response.getBody().get("MRData");
            Map<String, Object> raceTable = (Map<String, Object>) mrData.get("RaceTable");
            List<Object> races = (List<Object>) raceTable.get("Races");

            for(Object r: races){
                RaceResult newRaceResult = mapper.convertValue(r, RaceResult.class);
                resultsService.CreateRaceResult(newRaceResult);

            }
        }
    }
}
