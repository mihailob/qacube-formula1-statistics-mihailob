package qacube.internship.Backend.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qacube.internship.Backend.model.Constructor;
import qacube.internship.Backend.repository.ConstructorRepository;

import java.util.List;

@Service
public class ConstructorService {

    @Autowired
    private ConstructorRepository constructorRepository;

    public List<Constructor> findAll(){
        return constructorRepository.findAll();
    };
    public Constructor findOne(long constructorId){
        return constructorRepository.findById(constructorId).orElse(null);
    }

    public Constructor CreateConstructor(Constructor constructor){
        return constructorRepository.save(constructor);
    }

    public Constructor findByConstructorId(String constructorId){
        return constructorRepository.findByConstructorId(constructorId);
    }

}
