package qacube.internship.Backend.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qacube.internship.Backend.model.Circuit;
import qacube.internship.Backend.repository.CircuitRepository;

import java.util.List;

@Service
public class CircuitService {

    @Autowired
    private CircuitRepository circuitRepository;

    public List<Circuit> findAll(){
        return circuitRepository.findAll();
    };
    public Circuit findOne(long driverId){
        return circuitRepository.findById(driverId).orElse(null);
    }

    public void CreateCircuit(Circuit circuit){
        circuitRepository.save(circuit);
    }

    public Circuit findByCircuitId(String code){
        return circuitRepository.findByCircuitId(code);
    }
}

