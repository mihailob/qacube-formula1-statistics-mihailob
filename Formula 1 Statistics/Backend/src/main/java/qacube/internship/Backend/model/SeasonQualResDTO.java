package qacube.internship.Backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SeasonQualResDTO {

    private QualifyingResult minimumTime;

    private QualifyingResult maximumTime;

    private String averageTime;
}
