package qacube.internship.Backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class QualifyingResult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int number;

    private int position;

    @JsonProperty(value = "Driver")
    @OneToOne(fetch = FetchType.EAGER)
    private Driver driver;

    @JsonProperty(value = "Constructor")
    @ManyToOne(fetch = FetchType.EAGER)
    private Constructor constructor;

    @JsonProperty(value = "Q1")
    private String Q1;

    @JsonProperty(value = "Q2")
    private String Q2;

    @JsonProperty(value = "Q3")
    private String Q3;

}
