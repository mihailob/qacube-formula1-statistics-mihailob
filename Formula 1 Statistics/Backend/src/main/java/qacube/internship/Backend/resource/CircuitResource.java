package qacube.internship.Backend.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import qacube.internship.Backend.model.Circuit;
import qacube.internship.Backend.Service.CircuitService;

import java.util.List;
import java.util.Map;

@Component
public class CircuitResource {

    private static final String API_PREFIX = "https://ergast.com/api/f1/";

    private static final String API_CIRCUITS_POSTFIX = "/circuits.json";

    @Autowired
    private CircuitService circuitService;

    @Order(value = 3)
    @EventListener(ApplicationReadyEvent.class)
    public void loadCircuits(){

        RestTemplate restTemplate = new RestTemplate();

        ObjectMapper mapper = new ObjectMapper();

        for(int i = 2015; i<=2017; i++){
            ResponseEntity<Map> response =
                    restTemplate.getForEntity(
                            API_PREFIX + i + API_CIRCUITS_POSTFIX,
                            Map.class);

            Map<String, Object> mrData = (Map<String, Object>) response.getBody().get("MRData");
            Map<String, Object> circuitTable = (Map<String, Object>) mrData.get("CircuitTable");
            List<Object> circuits = (List<Object>) circuitTable.get("Circuits");

            for(Object circuit: circuits){
                Circuit newCircuit = mapper.convertValue(circuit, Circuit.class);
                if(circuitService.findByCircuitId(newCircuit.getCircuitId()) == null){
                    circuitService.CreateCircuit(newCircuit);
                }
            }
        }
    }
}
