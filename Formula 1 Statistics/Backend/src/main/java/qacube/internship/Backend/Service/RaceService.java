package qacube.internship.Backend.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qacube.internship.Backend.model.Race;
import qacube.internship.Backend.repository.RaceRepository;

import java.util.List;

@Service
public class RaceService {

    @Autowired
    private RaceRepository raceRepository;

    public List<Race> findAll(){
        return raceRepository.findAll();
    };
    public Race findOne(long raceId){
        return raceRepository.findById(raceId).orElse(null);
    }

    public void CreateRace(Race race){
        raceRepository.save(race);
    }
}
