package qacube.internship.Backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "race_results")
public class RaceResult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int number;

    private int position;

    private int points;

    @OneToOne
    private Driver driver;

    @ManyToOne(fetch = FetchType.EAGER)
    private Constructor constructor;

    private int grid;

    private int laps;

    private String status;

    @OneToOne(fetch = FetchType.EAGER)
    private Time time;

    @ManyToOne(fetch = FetchType.EAGER)
    private Race race;
}
