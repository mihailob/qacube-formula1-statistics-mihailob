package qacube.internship.Backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import qacube.internship.Backend.model.Driver;

@Repository
public interface DriverRepository extends JpaRepository<Driver, Long> {

    Driver findByCode(String code);
}
