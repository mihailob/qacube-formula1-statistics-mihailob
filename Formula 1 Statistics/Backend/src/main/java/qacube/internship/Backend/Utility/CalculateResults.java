package qacube.internship.Backend.Utility;

import qacube.internship.Backend.model.QualifyingResult;

import java.text.DecimalFormat;
import java.util.List;

public class CalculateResults {

    public static String CalculateAverageTime(List<QualifyingResult> qualifyingResultList){
        int counter = 0;
        double sum = 0;

        for(QualifyingResult qr : qualifyingResultList){
            double q1 = 0;
            double q2 = 0;
            double q3 = 0;

            if(qr.getQ1() != null){
                q1 = calculateQ(qr.getQ1());
                sum += q1;
                ++counter;
            }
            if(qr.getQ2() != null){
                q2 = calculateQ(qr.getQ2());
                sum += q2;
                ++counter;
            }
            if(qr.getQ3() != null) {
                q3 = calculateQ(qr.getQ3());
                sum += q3;
                ++counter;
            }
        }

        double averageInSeconds = sum/counter;
        int minutes = (int)averageInSeconds/60;
        double seconds = averageInSeconds % 60;

        DecimalFormat decimalFormatter = new DecimalFormat("#.000");

        return minutes + ":" + decimalFormatter.format(seconds);
    }

    public static QualifyingResult CalculateMinimumTime(List<QualifyingResult> qualifyingResultList){

        double minTime = 10000.0;
        QualifyingResult currentBest = new QualifyingResult();

        for(QualifyingResult qr: qualifyingResultList){
            double q1 = 0;
            double q2 = 0;
            double q3 = 0;

            if(qr.getQ1() == null || qr.getQ2() == null|| qr.getQ3() == null){
                continue;
            }

            q1 = calculateQ(qr.getQ1());
            q2 = calculateQ(qr.getQ2());
            q3 = calculateQ(qr.getQ3());

            if(q1 < minTime ){
                minTime = q1;
                currentBest = qr;
            }
            if(q2 < minTime ){
                minTime = q2;
                currentBest = qr;
            }
            if(q1 < minTime ){
                minTime = q3;
                currentBest = qr;
            }
        }

        return currentBest;
    }

    public static double calculateQ(String qx){
        double resultQ = 0;

        int minutesQx = Integer.parseInt(qx.split(":")[0]);
        double secondsQx = Double.parseDouble(qx.split(":")[1]);
        resultQ = secondsQx + minutesQx*60;

        return resultQ;
    }

    public static QualifyingResult CalculateMaximumTime(List<QualifyingResult> qualifyingResultList){

        double maxTime = 0.0;
        QualifyingResult currentWorst = new QualifyingResult();

        for(QualifyingResult qr: qualifyingResultList) {
            double q1 = 0.1;
            double q2 = 0.1;
            double q3 = 0.1;

            if (qr.getQ1() != null) {
                q1 = calculateQ(qr.getQ1());

                if (q1 > maxTime) {
                    maxTime = q1;
                    currentWorst = qr;
                }
            }
            if (qr.getQ2() != null) {
                q2 = calculateQ(qr.getQ2());

                if (q2 > maxTime) {
                    maxTime = q2;
                    currentWorst = qr;
                }
            }
            if (qr.getQ3() != null) {
                q3 = calculateQ(qr.getQ3());

                if (q3 > maxTime) {
                    maxTime = q3;
                    currentWorst = qr;
                }
            }
        }
        return currentWorst;
    }
}
