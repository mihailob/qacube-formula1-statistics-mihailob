package qacube.internship.Backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import qacube.internship.Backend.model.QualifyingResult;

@Repository
public interface QualifyingResultRepository extends JpaRepository<QualifyingResult, Long> {


}
