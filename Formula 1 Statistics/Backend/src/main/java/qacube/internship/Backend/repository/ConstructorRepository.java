package qacube.internship.Backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import qacube.internship.Backend.model.Constructor;

@Repository
public interface ConstructorRepository extends JpaRepository<Constructor, Long> {

    Constructor findByConstructorId(String constructorId);
}
