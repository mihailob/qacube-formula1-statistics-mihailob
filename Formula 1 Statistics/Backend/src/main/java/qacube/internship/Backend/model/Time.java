package qacube.internship.Backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Time {

    @Id
    private Long id;

    @OneToOne(mappedBy = "time",fetch = FetchType.EAGER)
    private RaceResult raceResult;

    private int mills;

    private String time;


}
