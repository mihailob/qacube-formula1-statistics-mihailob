package qacube.internship.Backend.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qacube.internship.Backend.model.QualifyingResult;
import qacube.internship.Backend.model.Race;
import qacube.internship.Backend.repository.QualifyingResultRepository;
import qacube.internship.Backend.repository.RaceRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class QualifyingResultsService {

    @Autowired
    private QualifyingResultRepository qualifyingResultRepository;

    @Autowired
    private RaceRepository raceRepository;

    public List<QualifyingResult> findAll(){
        return qualifyingResultRepository.findAll();
    };
    public QualifyingResult findOne(long resultId){
        return qualifyingResultRepository.findById(resultId).orElse(null);
    }

    public QualifyingResult CreateRaceResult(QualifyingResult qualifyingResult){
        return qualifyingResultRepository.save(qualifyingResult);
    }

    public List<QualifyingResult> getResultsForSeason(int season){
        List<QualifyingResult> allSeasonQualRes = new ArrayList<>();
        List<Race> races = raceRepository.findAllBySeason(season);
        for(Race r: races){
            allSeasonQualRes.addAll(r.getQualifyingResults());
        }
        return allSeasonQualRes;
    }

}
