package qacube.internship.Backend.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qacube.internship.Backend.model.Driver;
import qacube.internship.Backend.Service.DriverService;

import java.util.List;


@Component
public class DriverResolver implements GraphQLQueryResolver {

    @Autowired
    private DriverService driverService;

    public List<Driver> drivers(){
        return driverService.findAll();

    }
    public Driver driver(String code){
        return driverService.findByCode(code);

    }
}
