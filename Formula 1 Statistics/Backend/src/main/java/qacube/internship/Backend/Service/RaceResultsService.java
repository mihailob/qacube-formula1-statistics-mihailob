package qacube.internship.Backend.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qacube.internship.Backend.model.RaceResult;
import qacube.internship.Backend.repository.RaceResultRepository;

import java.util.List;

@Service
public class RaceResultsService {

    @Autowired
    private RaceResultRepository resultRepository;

    public List<RaceResult> findAll(){
        return resultRepository.findAll();
    };
    public RaceResult findOne(long resultId){
        return resultRepository.findById(resultId).orElse(null);
    }

    public void CreateRaceResult(RaceResult result){
        resultRepository.save(result);
    }


}
