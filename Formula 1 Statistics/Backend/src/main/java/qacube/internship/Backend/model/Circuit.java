package qacube.internship.Backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Circuit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String circuitId;

    private String url;

    private String circuitName;

    @JsonProperty("Location")
    @ManyToOne(cascade = {CascadeType.ALL})
    private Location location;

}
