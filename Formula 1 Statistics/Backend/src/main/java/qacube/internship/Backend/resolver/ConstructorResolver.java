package qacube.internship.Backend.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qacube.internship.Backend.model.Constructor;
import qacube.internship.Backend.Service.ConstructorService;

import java.util.List;

@Component
public class ConstructorResolver implements GraphQLQueryResolver{

    @Autowired
    private ConstructorService constructorService;

    public List<Constructor> constructors(){
        return constructorService.findAll();

    }
    public Constructor constructor(String constructorId){
        return constructorService.findByConstructorId(constructorId);
    }
}
