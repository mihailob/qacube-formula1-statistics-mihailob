package qacube.internship.Backend.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import qacube.internship.Backend.model.QualifyingResult;
import qacube.internship.Backend.model.SeasonQualResDTO;

public class QualifyingResultResolver implements GraphQLResolver<SeasonQualResDTO> {

    public QualifyingResult maximumTime(SeasonQualResDTO seasonQualResDTO) {
        return seasonQualResDTO.getMaximumTime();
    }

    public QualifyingResult minimumTime(SeasonQualResDTO seasonQualResDTO) {
        return seasonQualResDTO.getMaximumTime();
    }
}

