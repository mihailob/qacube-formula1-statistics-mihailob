package qacube.internship.Backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Driver {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String driverId;

    private int permanentNumber;

    private String code;

    private String url;

    private String givenName;

    private String familyName;

    private String dateOfBirth;

    private String nationality;

    @OneToOne(fetch = FetchType.EAGER)
    private RaceResult raceResult;

    @OneToOne(fetch = FetchType.EAGER)
    private QualifyingResult qualifyingResult;
}
