package qacube.internship.Backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String lat;

    @JsonProperty("long")
    private String longitude;

    private String locality;

    private String country;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<Circuit> circuit;

}
