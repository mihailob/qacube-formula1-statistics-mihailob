package qacube.internship.Backend.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qacube.internship.Backend.model.Driver;
import qacube.internship.Backend.repository.DriverRepository;

import java.util.List;

@Service
public class DriverService {

    @Autowired
    private DriverRepository driverRepository;

    public List<Driver> findAll(){
        return driverRepository.findAll();
    };
    public Driver findOne(long driverId){
        return driverRepository.findById(driverId).orElse(null);
    }

    public void CreateDriver(Driver driver){
        driverRepository.save(driver);
    }

    public Driver findByCode(String code){
        return driverRepository.findByCode(code);
    }
}
