package qacube.internship.Backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import qacube.internship.Backend.model.QualifyingResult;
import qacube.internship.Backend.model.SeasonQualResDTO;
import qacube.internship.Backend.Service.QualifyingResultsService;
import qacube.internship.Backend.Utility.CalculateResults;

import java.util.List;

@RestController
@RequestMapping("/f1")
public class MainController {

    @Autowired
    private QualifyingResultsService qualifyingResultsService;

    @RequestMapping(value="/{season}" ,method = RequestMethod.GET)
    public ResponseEntity<SeasonQualResDTO> seasonInfo(@PathVariable int season){

        SeasonQualResDTO seasonInfo = new SeasonQualResDTO();

        List<QualifyingResult> qualifyingResultsSeason = qualifyingResultsService.getResultsForSeason(season);
        seasonInfo.setAverageTime(CalculateResults.CalculateAverageTime(qualifyingResultsSeason));
        seasonInfo.setMinimumTime(CalculateResults.CalculateMinimumTime(qualifyingResultsSeason));
        seasonInfo.setMaximumTime(CalculateResults.CalculateMaximumTime(qualifyingResultsSeason));

        return new ResponseEntity<>(seasonInfo, HttpStatus.OK);
    }

}
