package qacube.internship.Backend.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import qacube.internship.Backend.model.Constructor;
import qacube.internship.Backend.Service.ConstructorService;

import java.util.List;
import java.util.Map;

@Component
public class ConstructorResource {

    private static final String API_PREFIX = "https://ergast.com/api/f1/";

    private static final String API_CONSTRUCTORS_POSTFIX = "/constructors.json";

    @Autowired
    private ConstructorService constructorService;

    @Order(value = 2)
    @EventListener(ApplicationReadyEvent.class)
    public void loadConstructors(){
        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();

        for(int i = 2015; i<=2017; i++){
            ResponseEntity<Map> response =
                    restTemplate.getForEntity(
                            API_PREFIX + i + API_CONSTRUCTORS_POSTFIX,
                            Map.class);

            Map<String, Object> mrData = (Map<String, Object>) response.getBody().get("MRData");
            Map<String, Object> constructorTable = (Map<String, Object>) mrData.get("ConstructorTable");
            List<Object> constructors = (List<Object>) constructorTable.get("Constructors");

            for(Object constructor: constructors){
                Constructor newConstructor = mapper.convertValue(constructor, Constructor.class);
                if(constructorService.findByConstructorId(newConstructor.getConstructorId()) == null){
                    constructorService.CreateConstructor(newConstructor);
                }
            }
        }
    }
}
