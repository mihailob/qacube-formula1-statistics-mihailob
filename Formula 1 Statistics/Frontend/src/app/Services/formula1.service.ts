import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class Formula1Service {

  constructor(private http: HttpClient) { }

  public getQualifyResultsSimple(year: number): any{
    const queryString: string = JSON.stringify({
      query: `
        query { qualifyingResultsBySeason(season:` + year + `){
          averageTime,
          minimumTime{
             driver{
                  givenName,
                  familyName,
                  nationality,
                  code}
          },
          maximumTime{
              driver{
                  givenName,
                  familyName,
                  nationality,
                  code}
          }
        }
        }
      `});
    return this.http.post('http://localhost:8080/graphql', queryString).toPromise();
  }

  public getQualifyResultsDetails(year: number): any{
    const queryString: string = JSON.stringify({
      query: `
        query { qualifyingResultsBySeason(season:` + year + `){
          averageTime,
          minimumTime{
            driver{
                givenName,
                familyName,
                nationality,
                driverId
            },
            constructor{
                constructorId,
                nationality,
                name},
            Q1,
            Q2,
            Q3
          },
          maximumTime{
            driver{
                givenName,
                familyName,
                nationality,
                driverId
                },
            constructor{
                constructorId,
                nationality,
                name
              },
            Q1,
            Q2,
            Q3
          }
        }
        }
      `});
    return this.http.post('http://localhost:8080/graphql', queryString).toPromise();
  }
}

