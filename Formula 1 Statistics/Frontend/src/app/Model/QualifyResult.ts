import {Driver} from './Driver';
import {Constructor} from './Constructor';

export interface QualifyResult{
  number: number;
  position: number;
  Driver: Driver;
  Constructor: Constructor;
  Q1: string;
  Q2: string;
  Q3: string;
}
