import {QualifyResult} from './QualifyResult';

export interface Result {
  minimumTime: QualifyResult;
  maximumTime: QualifyResult;
  averageTime: string;
}
