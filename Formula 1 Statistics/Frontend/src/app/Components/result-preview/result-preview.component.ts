import {Component, Input, OnInit, Output} from '@angular/core';
import {Result} from '../../Model/Result';
import { EventEmitter } from '@angular/core';
import {Constructor} from '../../Model/Constructor';
import {Driver} from '../../Model/Driver';
import {QualifyResult} from '../../Model/QualifyResult';

@Component({
  selector: 'app-result-preview',
  templateUrl: './result-preview.component.html',
  styleUrls: ['./result-preview.component.css']
})
export class ResultPreviewComponent implements OnInit {
  minTimeConstructor: Constructor ;
  minTimeDriver: Driver;
  minTimeResult: { Q1: string; Q2: string; number: number; Q3: string; position: number } = {
    Q1: '', Q2: '', Q3: '', number: 0, position: 0

  };

  maxTimeConstructor: Constructor;
  maxTimeDriver: Driver;
  maxTimeResult: { Q1: string; Q2: string; number: number; Q3: string; position: number } = {
    Q1: '', Q2: '', Q3: '', number: 0, position: 0

  };

  constructor() {

  }

  @Output() back = new EventEmitter<boolean>();
  @Input() Result: Result;

  ngOnInit(): void {


    this.minTimeDriver = this.Result.minimumTime.Driver;
    this.minTimeConstructor = this.Result.minimumTime.Constructor;
    if (this.Result.minimumTime.Q1){
      this.minTimeResult.Q1 = this.Result.minimumTime.Q1;
    }
    if (this.Result.minimumTime.Q2){
      this.minTimeResult.Q2 = this.Result.minimumTime.Q2;
    }
    if (this.Result.minimumTime.Q3){
      this.minTimeResult.Q3 = this.Result.minimumTime.Q3;
    }

    this.maxTimeDriver = this.Result.maximumTime.Driver;
    this.maxTimeConstructor = this.Result.maximumTime.Constructor;
    if (this.Result.maximumTime.Q1){
      this.maxTimeResult.Q1 = this.Result.maximumTime.Q1;
    }
    if (this.Result.maximumTime.Q2){
      this.maxTimeResult.Q2 = this.Result.maximumTime.Q2;
    }
    if (this.Result.maximumTime.Q3){
      this.maxTimeResult.Q3 = this.Result.maximumTime.Q3;
    }
  }

  goToHomePage(): any {
    this.back.emit(true);
  }
}
