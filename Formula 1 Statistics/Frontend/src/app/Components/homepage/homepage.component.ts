import { Component, OnInit } from '@angular/core';
import {Formula1Service} from '../../Services/formula1.service';
import {Result} from '../../Model/Result';
import {Router} from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  public result: Result;
  yearChooserVisible: boolean;
  public season: number;
  constructor(private f1service: Formula1Service, private router: Router) { }

  ngOnInit(): void {
    this.yearChooserVisible = true;
  }

  public async getQualifyResultsForYear(year: number): Promise<any>{
    try{
      const response = await this.f1service.getQualifyResultsSimple(year) as any;
      this.result = response.data.qualifyingResultsBySeason;
      this.season = year;
      console.log(this.result);
      this.yearChooserVisible = false;
    }catch (error){
      alert('Oops, something went wrong!');
    }

  }

  showChooser(showChooser: boolean): any{
    this.yearChooserVisible = showChooser;
  }
}
